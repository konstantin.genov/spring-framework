# Spring Framework

I have made this repository to keep track of my progress as I refresh my knowledge and deepen my understanding of the Spring Framework.  

Resources are split on different branches, the **master branch** will be kept **empty**.
The readme file will be updated constantly as I progress through Spring.

List of branches: 
1. Configuring Spring Applications - [01-Spring-Configurations](https://gitlab.com/konstantin.genov/spring-framework/-/tree/01-spring-confugirations)
2. Setting up an application with Spring Boot, persistance and RESTful services - [02-Spring-Boot-Basics](https://gitlab.com/konstantin.genov/spring-framework/-/tree/02-spring-boot-basics)
3. Using Spring Actuator to monitor and manage your applications - [03-Spring-Boot-Actuator](https://gitlab.com/konstantin.genov/spring-framework/-/tree/03-spring-boot-actuator)
